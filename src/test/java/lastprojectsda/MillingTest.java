package lastprojectsda;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import pl.mati.last.test.TollRotation;

public class MillingTest {
	private TollRotation calc;

	@Before
	public void setup() {
		calc = new TollRotation();
	}


	@SuppressWarnings("deprecation")
	@Test
	public void Rotation() {
		assertEquals(1, calc.calculateToolRotation(200, 200));
	}
}
