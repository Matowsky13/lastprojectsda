package pl.mati.last.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;


@Entity
public class Laser {
	@Id
	@GeneratedValue
	private long id;
	@Column(name="thickness")
	private double thickness;
	@Column(name="speed")
	private int speed;
	@ManyToOne (cascade = CascadeType.MERGE)
	private TypeOfMaterialLaser typeOfMaterialLaser;
	
	public Laser() {
	}

	public Laser(double thickness, int speed) {
		this.thickness = thickness;
		this.speed = speed;

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getThickness() {
		return thickness;
	}

	public void setThickness(double thickness) {
		this.thickness = thickness;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public TypeOfMaterialLaser getTypeOfMaterialLaser() {
		return typeOfMaterialLaser;
	}

	public void setTypeOfMaterialLaser(TypeOfMaterialLaser typeOfMaterialLaser) {
		this.typeOfMaterialLaser = typeOfMaterialLaser;
	}
	


}
