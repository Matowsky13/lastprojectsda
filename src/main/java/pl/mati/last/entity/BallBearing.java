package pl.mati.last.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity (name="BALLBEARING")
public class BallBearing {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
private int Id;
private int ballSeries;
private String smalldimension;
private int bigDimension;
private int width;
private double radius;
private double heigh;
private int seriesNumber;
public BallBearing() {
	super();
}
public int getId() {
	return Id;
}
public void setId(int id) {
	Id = id;
}
public int getBallSeries() {
	return ballSeries;
}
public void setBallSeries(int ballSeries) {
	this.ballSeries = ballSeries;
}
public String getSmalldimension() {
	return smalldimension;
}
public void setSmalldimension(String smalldimension) {
	this.smalldimension = smalldimension;
}
public int getBigDimension() {
	return bigDimension;
}
public void setBigDimension(int bigDimension) {
	this.bigDimension = bigDimension;
}
public int getWidth() {
	return width;
}
public void setWidth(int width) {
	this.width = width;
}
public double getRadius() {
	return radius;
}
public void setRadius(double radius) {
	this.radius = radius;
}
public double getHeigh() {
	return heigh;
}
public void setHeigh(double heigh) {
	this.heigh = heigh;
}
public int getSeriesNumber() {
	return seriesNumber;
}
public void setSeriesNumber(int seriesNumber) {
	this.seriesNumber = seriesNumber;
}

public BallBearing(int ballSeries, String smalldimension, int bigDimension, int width, double radius, double heigh,
		int seriesNumber) {
	super();
	this.ballSeries = ballSeries;
	this.smalldimension = smalldimension;
	this.bigDimension = bigDimension;
	this.width = width;
	this.radius = radius;
	this.heigh = heigh;
	this.seriesNumber = seriesNumber;
}
@Override
public String toString() {
	return "BallBearing [Id=" + Id + ", ballSeries=" + ballSeries + ", smalldimension=" + smalldimension
			+ ", bigDimension=" + bigDimension + ", width=" + width + ", radius=" + radius + ", heigh=" + heigh
			+ ", seriesNumber=" + seriesNumber + "]";
}


}
