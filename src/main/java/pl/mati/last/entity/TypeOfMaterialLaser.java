package pl.mati.last.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class TypeOfMaterialLaser {

	@Id
	@GeneratedValue
	private long id;
	@Column(name = "typeOfMaterial")
	private String typeOfMaterial;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "typeOfMaterialLaser", cascade = CascadeType.ALL)
	private List<Laser> listOflaser;

	public TypeOfMaterialLaser() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTypeOfMaterial() {
		return typeOfMaterial;
	}

	public void setTypeOfMaterial(String typeOfMaterial) {
		this.typeOfMaterial = typeOfMaterial;
	}

	public List<Laser> getListOflaser() {
		return listOflaser;
	}

	public void setListOflaser(List<Laser> listOflaser) {
		this.listOflaser = listOflaser;
	}



}
