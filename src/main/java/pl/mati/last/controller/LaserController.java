package pl.mati.last.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.mati.last.entity.Laser;
import pl.mati.last.entity.TypeOfMaterialLaser;
import pl.mati.last.repository.LaserRepository;
import pl.mati.last.repository.TypeOfMaterialLaserRepository;


@Controller

@RequestMapping("/machine")
public class LaserController {
	@Autowired
	TypeOfMaterialLaserRepository typeOfMaterialLaserRepository;
	@Autowired
	LaserRepository laserRepository;
	
	@RequestMapping(value = "/laser", method = RequestMethod.GET)
	public String laserGet(ModelMap model) {
		List<TypeOfMaterialLaser> typeOfMaterialLaser = (List<TypeOfMaterialLaser>) typeOfMaterialLaserRepository.findAll();
		model.addAttribute("typeOfMaterialLaser",typeOfMaterialLaser);
		model.addAttribute("laser",laserRepository.findAll());
		return "/machine/laser";
	}
	@RequestMapping(value = "/laser", method = RequestMethod.POST)
	public String laserPost(@RequestParam("thickness") double thickness, @RequestParam("speed") int speed, @RequestParam("length") double length, ModelMap model,@RequestParam("typeOfMaterialLaser") long typeId) {
		
		TypeOfMaterialLaser typeOfMaterialLaser = typeOfMaterialLaserRepository.findOne(typeId);
		Laser ls=new Laser();
		ls.setTypeOfMaterialLaser(typeOfMaterialLaser);
		ls.setThickness(thickness);
		ls.setSpeed(speed);
		
		laserRepository.save(ls);
		double cuttingTime = length/speed;
		model.addAttribute("cuttingTime",cuttingTime);
		
	
		return "/machine/laser";
	}
	
}
