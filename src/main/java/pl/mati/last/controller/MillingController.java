package pl.mati.last.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/other")
public class MillingController {
	 public static final double PI = 3.14;
	 public static final int STATIC_PARAM = 1000;

	@RequestMapping(value = "/milling", method = RequestMethod.GET)
	public String millingGet(ModelMap model) {
		model.addAttribute("toolsRotation",0);
		model.addAttribute("toolSpeed",0);
		model.addAttribute("toolDimension",0);
		return "/other/milling";
	}
	@RequestMapping(value = "/milling", method = RequestMethod.POST)
	public String millingPost(@RequestParam("toolSpeed") double toolSpeed, @RequestParam("toolDimension") double toolDimension, ModelMap model) {
		
		double toolsRotation = (toolSpeed*STATIC_PARAM)/PI*toolDimension;
		model.addAttribute("toolsRotation",toolsRotation);
		model.addAttribute("toolSpeed",toolSpeed);
		model.addAttribute("toolDimension",toolDimension);
		
	
		return "/other/milling";
	}
}
