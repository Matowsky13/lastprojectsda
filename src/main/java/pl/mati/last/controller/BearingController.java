package pl.mati.last.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.mati.last.entity.BallBearing;
import pl.mati.last.repository.BallBearingRepository;

@Controller
@RequestMapping("/bearing")
public class BearingController {
	@Autowired
	BallBearingRepository ballBearingRepository;

	@RequestMapping(value = "/ballbearing", method = RequestMethod.GET)
	public String ballBearingGet(ModelMap model) {
		List<BallBearing> ballBearings = (List<BallBearing>) ballBearingRepository.findAll();
		model.addAttribute("ballBearings", ballBearings);
		return "/bearing/ballbearing";
	}

	@RequestMapping(value = "/ballbearing", method = RequestMethod.POST)
	public String ballBearingPost(ModelMap model) {
		return "/bearing/ballbearing";
	}

	@RequestMapping(value = "/ballbearingfilter", method = RequestMethod.GET)
	public String ballBearingFilterGet(ModelMap model) {
		;
		return "/bearing/ballbearingfilter";
	}

	@RequestMapping(value = "/ballbearingfilter", method = RequestMethod.POST)
	public void ballBearingFilterPost(@RequestParam(value = "search") String search, ModelMap model,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		if (search.isEmpty()) {
			response.sendRedirect(request.getContextPath() + "/bearing/ballbearing");

		} else {
			List<BallBearing> bearings = (List<BallBearing>) ballBearingRepository.findBySmallDimension(search);
			model.addAttribute("ballBearing", bearings);

			response.sendRedirect(request.getContextPath() + "/bearing/ballbearingfilter");

		}

	}

}
