package pl.mati.last.controller;


import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.mati.last.entity.RegistrationPanel;
import pl.mati.last.repository.RegistrationPanelRepository;

@Controller
@RequestMapping("/registry")
public class RegistationController {
	@Autowired
	RegistrationPanelRepository registationPanelRepository;
	
	@RequestMapping(value = "/registry", method = RequestMethod.GET)
	public String registrationGet(ModelMap model) {
		return "/registry/registry";
	}
	@RequestMapping(value = "/registry", method = RequestMethod.POST)
	public void registrationPost(ModelMap model,HttpServletRequest request, HttpServletResponse response,@RequestParam("username") String username,@RequestParam("password") String password,@RequestParam("name") String name,@RequestParam("surname") String surname,@RequestParam("email") String email) throws IOException {
		RegistrationPanel registationPanel = new RegistrationPanel(username, password, name, surname, email);
		registationPanelRepository.save(registationPanel);
		response.sendRedirect(request.getContextPath() + "/");
	}
}
