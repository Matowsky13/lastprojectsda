package pl.mati.last.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.mati.last.repository.RegistrationPanelRepository;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/login")
public class LoginController {
	@Autowired
	RegistrationPanelRepository registrationPanelRepository;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		return "/login/login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public void loginPost(@RequestParam(value = "username") String username,@RequestParam(value = "password") String password, HttpServletRequest request, HttpServletResponse response,ModelMap
			 model)
			throws ServletException, IOException {
		String name = registrationPanelRepository.findByUserName(username);
		String pass =registrationPanelRepository.findByPass(password);
		if (username.equals(name) && password.equals(pass)) {
			request.getSession().setAttribute("loggedin", true);
		}
		else {
			String msg = "Niepoprawne dane logowania";
			model.addAttribute("message",msg);
		}
		response.sendRedirect(request.getContextPath() + "/");
	}
	@RequestMapping(value="/logout", method = RequestMethod.GET)
	public void logoutPagePost (HttpServletRequest request, HttpServletResponse response) throws IOException {
		request.getSession().removeAttribute("loggedin");
		response.sendRedirect(request.getContextPath() + "/");
	}

}
