package pl.mati.last.repository;

import org.springframework.data.repository.CrudRepository;

import pl.mati.last.entity.Laser;

public interface LaserRepository extends CrudRepository<Laser, Long> {

}
