package pl.mati.last.repository;

import org.springframework.data.repository.CrudRepository;

import pl.mati.last.entity.TypeOfMaterialLaser;

public interface TypeOfMaterialLaserRepository  extends CrudRepository<TypeOfMaterialLaser, Long>{

}
