package pl.mati.last.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import pl.mati.last.entity.RegistrationPanel;

public interface RegistrationPanelRepository extends CrudRepository<RegistrationPanel, Long> {

	@Query("SELECT username FROM REGISTRY WHERE username = :username")
	String findByUserName(@Param("username") String username);
	
	@Query("SELECT password FROM REGISTRY WHERE password = :password")
	String findByPass(@Param("password") String password);
}
