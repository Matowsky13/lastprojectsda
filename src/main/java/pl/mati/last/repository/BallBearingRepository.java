package pl.mati.last.repository;




import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import pl.mati.last.entity.BallBearing;

public interface BallBearingRepository extends CrudRepository<BallBearing, Long>{

	@Query("FROM BALLBEARING WHERE smalldimension = :search")
	Iterable<BallBearing> findBySmallDimension(@Param("search") String search);
	
}
