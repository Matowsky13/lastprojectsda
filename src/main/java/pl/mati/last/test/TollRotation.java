package pl.mati.last.test;

public class TollRotation {
	 public static final double PI = 3.14;
	 public static final int STATIC_PARAM = 1000;
	 
	 private double toolsRotation;
	 private double toolSpeed;
	 private double toolDimension;
	public TollRotation(double toolSpeed, double toolDimension) {
		super();
		this.toolSpeed = toolSpeed;
		this.toolDimension = toolDimension;
	}
	
	public TollRotation() {
		super();
	}

	public double getToolsRotation() {
		return toolsRotation;
	}
	public void setToolsRotation(double toolsRotation) {
		this.toolsRotation = toolsRotation;
	}
	public double getToolSpeed() {
		return toolSpeed;
	}
	public void setToolSpeed(double toolSpeed) {
		this.toolSpeed = toolSpeed;
	}
	public double getToolDimension() {
		return toolDimension;
	}
	public void setToolDimension(double toolDimension) {
		this.toolDimension = toolDimension;
	}
	
	public double calculateToolRotation(double toolSpeed,double toolDimension)  {
		double toolsRotation = (toolSpeed)/(toolDimension);
		return toolsRotation;
	}
	 
	 
}
