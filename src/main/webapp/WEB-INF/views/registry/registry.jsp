<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:import url="../header.jsp" />

<form action='' method="POST">
	<c:if test="${msg}">
		<div class="alert alert-success">
			<p>${msg}</p>
		</div>
	</c:if>
	<div class="input-group">
		<span class="input-group-addon">Użytkownik:</span> <input
			type="text" name="username">
	</div>
	<div class="input-group">
		<span class="input-group-addon">Hasło:</span> <input type="password"
			name="password">
	</div>
	<div class="input-group">
		<span class="input-group-addon">Imię:</span> <input type="text"
			name="name">
	</div>
	<div class="input-group">
		<span class="input-group-addon">Nazwisko:</span> <input type="text"
			name="surname">
	</div>
		<div class="input-group">
		<span class="input-group-addon">Adres e-mail:</span> <input type="text"
			name="email">
	</div>

	<button type="submit">Dodaj użytkownika</button>

</form>

<c:import url="../footer.jsp" />