<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
<link rel="stylesheet" href="<c:url value='/css/login.css'/>">
</head>
<body>

	<form action='<c:url value="/login/login"/>' method="POST">

		<input type="text" name="username" placeholder="Podaj login" /> <input
			type="password" name="password" placeholder="Podaj hasło" />

		<button type="submit">Zaloguj</button>
	</form>
</body>
</html>
	<c:if test="${message ne null}">
		<div class="alert alert-danger">
			<strong>${message}</strong>
		</div>
		</c:if>