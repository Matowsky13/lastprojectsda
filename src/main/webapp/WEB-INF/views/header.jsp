<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Enginee..support</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">
<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href='<c:url value="/"/>'>Home</a>
			</div>
			<c:if test="${sessionScope.loggedin}">
				<div class="collapse navbar-collapse"
					id="bs-example-navbar-collapse-1">

					<ul class="nav navbar-nav">
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown" role="button" aria-haspopup="true"
							aria-expanded="false">Machine <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href='<c:url value="/machine/laser"/>'>Laser beam</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="#">UHP</a></li>
							</ul></li>
							
								<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown" role="button" aria-haspopup="true"
							aria-expanded="false">Bearings <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href='<c:url value="/bearing/ballbearing"/>'>Ball</a></li>
							</ul></li>
							
								<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown" role="button" aria-haspopup="true"
							aria-expanded="false">CNC Machines <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href='<c:url value="/other/milling"/>'>Milling machine</a></li>
							</ul></li>
							
					
							
						<li><a href="<c:url value="login/logout" />">Logout<span
								class="glyphicon glyphicon-log-out"></span></a></li>

					</ul>
				</div>
			</c:if>
			<ul class="nav navbar-nav">
			<li><c:if test="${sessionScope.loggedin ne true}">
					<a href='<c:url value="/login/login"/>'><span
						class="glyphicon glyphicon-log-in"></span>Login</a>
						<li>
					<a href='<c:url value="/registry/registry"/>'><span
						class="glyphicon glyphicon-log-in"></span>Rejestracja</a>
				</li>
				</c:if></li>
				
		</ul>
		</div>
		
	</nav>
	<c:if test="${sessionScope.loggedin}">
		<div class="alert alert-success">
			<p>Jesteś zalogowany</p>
		</div>
	</c:if>
	
	<c:if test="${message ne null}">
		<div class="alert alert-danger">
			<strong>${message}</strong>
		</div>
		</c:if>