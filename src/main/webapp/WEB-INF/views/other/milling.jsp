<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<c:import url="../header.jsp" />
<form action='' method="POST">
	<h1 align="center">Kalkulator frezowania CNC</h1>
	<img
		src="http://artykulytechniczne.pl/blog/wp-content/uploads/2015/07/09thumbnail.jpg"
		class="img-rounded" alt="Cinque Terre" width="250" height="190" style="margin-left:640px">
	<div class="container" align="center">
		<span class="input-group-addon">Prędkość skrawania [m/min]:</span> <input
			type="text" name="toolSpeed" value="${toolSpeed}" class="form-control" style="text-align: center">
	</div>
	<div class="container" align="center">
		<span class="input-group-addon">Średnica narzędzia [mm]:</span> <input
			type="text" name="toolDimension" value="${toolDimension}"  class="form-control" style="text-align: center">
	</div>

	<fmt:formatNumber type="number" maxFractionDigits="2" value="${toolsRotation}" var="z"/>
	<div class="container" align="center">
		<span class="input-group-addon">Obroty narzędzia [obr/min]</span><input
			type="number" name="toolsRotation" value="${toolsRotation}" 
			maxlength="5" size="10" disabled="disabled"
			style="width: 200px; text-align: center;"><span>s</span>
	</div>
	<div class="container" align="center" style="margin-top: 20px">
		<button type="submit" class="btn btn-success"
			style="width: 200px; align-items: center;">Oblicz obroty narzędzia</button>
	</div>

</form>
<c:import url="../footer.jsp" />
