<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<head>
<link rel="stylesheet" href="<c:url value='/css/laser.css'/>">
</head>
<c:import url="../header.jsp" />
<form action='' method="POST">
	<c:if test="${msg}">
		<div class="alert alert-success">
			<p>${msg}</p>
		</div>
	</c:if>

	<h1 align="center">Kalkulator cięcia laserem CNC</h1>
	<div class="container" align="center">
		<span class="input-group-addon">Typ Materiału:</span> <select
			name="typeOfMaterial"  id="materialSelect" onchange="myFunction()" style="width: 200px; text-align: center">
			<option value="0">Brak wybranego materiału</option>
			<c:forEach items="${typeOfMaterialLaser}" var="typeOfLaser">
				<option value="${typeOfLaser.id}" id="typ">${typeOfLaser.typeOfMaterial}</option>
			</c:forEach>
		</select>
		
	</div>
	<div class="container" align="center">
		<span class="input-group-addon">Grubość:</span> <select
			name="thickness" style="width: 200px; text-align: center">
			<option>Wybierz materiał</option>
			
			<c:forEach items="${laser}" var="laser">
				<c:if test="${laser.typeOfMaterialLaser.id eq 2 }" >
					<option value="${laser.id}">${laser.thickness}</option>
				</c:if>
			</c:forEach>
		</select>
	</div>
	<div class="container" align="center">
		<span class="input-group-addon">Prędkość:</span>

		<c:forEach items="${lr}" var="lr">
			<c:if test="${laser.typeOfMaterialLaser.id eq x } ">
				<input type="text" name="speed" value="${lr.speed}"
					style="width: 200px; margin-right 1500px; text-align: center;"
					disabled="disabled">
				<span style="margin-left: 5px">mm/s</span>
			</c:if>
		</c:forEach>
	</div>
	<div class="container" align="center">
		<span class="input-group-addon">Długość:</span> <input type="text"
			name="length" value="" style="width: 200px; text-align: center;"><span>mm</span>
	</div>
	<div class="container" align="center">
		<span class="input-group-addon">Wynik</span><input type="text"
			name="timeOfCutting" value="${cuttingTime}" maxlength="5" size="10"
			disabled="disabled" style="width: 200px; text-align: center;"><span>s</span>
	</div>
	<div class="container" align="center" style="margin-top: 20px">
		<button type="submit" class="btn btn-success"
			style="width: 200px; align-items: center;">>Oblicz</button>
	</div>
	
</form>

<form action='<c:url value="/machine/laser"/>' method="GET">
	<div class="container" align="center" style="margin-top: 20px">
		<button type="submit" class="btn btn-default" style="width: 200px;">>Odśwież</button>
	</div>
</form>


<c:import url="../footer.jsp" />
<script>
function myFunction() {
	var x = document.getElementById("materialSelect").value;
    document.getElementById("demo").innerHTML = "You selected: " + x;

}
</script>