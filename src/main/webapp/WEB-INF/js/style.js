function myFunction() {
    // Declare variables
    var input, filter, tr,td, a, i;
    input = document.getElementById('searchBearing');
    filter = input.value.toUpperCase();
    tr = document.getElementById("bBall");
    td = tr.getElementsByTagName('td');

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < td.length; i++) {
        a = td[i].getElementsByTagName("a")[0];
        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            td[i].style.display = "";
        } else {
            td[i].style.display = "none";
        }
    }
}
